/**
 * @file main.c
 * @author Matías S. Ávalos (@tute_avalos)
 * @brief Ejemplo de PWM haciendo un degradé con un RGB
 * @version 0.1
 * @date 2020-07-11
 * 
 * En este ejemplo se configuran las salidas T4C2/3/4 (PB7/8/9) como
 * PWM alineado al centro para causar un degradé de colores sobre un
 * LED RGB ánodo común.
 * 
 * @copyright Copyright (c) 2020
 * 
 * MIT License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/systick.h>

#define MAX_COUNT 10000 // Máximo valor a contar del PWM.

volatile uint32_t millis;

/**
 * @brief Delay bloqueante utilizando el SysTick
 * 
 * @param ms duración de la demora
 */
void delay_ms(uint32_t ms)
{
    uint32_t lm = millis;
    while ((millis - lm) < ms);
}

int main()
{
    // CPU = 72Mhz; APB1 = 36Mhz; APB2 = 72Mhz
    rcc_clock_setup_in_hse_8mhz_out_72mhz();

    /* Se configura el Systick para interrumpir c/1ms */
    systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8);
    systick_set_reload(8999);
    systick_interrupt_enable();
    systick_counter_enable();

    /* Se configuran los pines de los puertos correspondientes */
    rcc_periph_clock_enable(RCC_GPIOB);

    // PB7, PB8 y PB9 => TIM4_CH2, TIM4_CH3 y TIM4_CH4
e(
        GPIOB,                          // Puerto correspondiente
        GPIO_MODE_OUTPUT_2_MHZ,         // Máxima velocidad de switcheo
        GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, // Función alternativa
        GPIO7 | GPIO8 | GPIO9);         // Pines asociados al OC2, OC3 y OC4

    /* Se configura el TIM como PWM alineado al centro */
    rcc_periph_clock_enable(RCC_TIM4);

    timer_set_mode(
        TIM4,                 // Timer general 4
        TIM_CR1_CKD_CK_INT,   // Clock interno como fuente
        TIM_CR1_CMS_CENTER_1, // Modo centrado
        TIM_CR1_DIR_UP);      // Indistinto, esto es ignorado...

    /*  Seteamos la cuenta del Timer
        
    er
        cuenta para arriba y luego para abajo, por lo tanto, debemos
        dividir la frecuencia x2.
    */
    timer_set_period(TIM4, MAX_COUNT - 1); // 72M/2/10000 = 3,6khz

    // Configuramos las salidas del timer:
    timer_set_oc_mode(TIM4, TIM_OC2, TIM_OCM_PWM2); // PWM2: active LOW
    timer_set_oc_mode(TIM4, TIM_OC3, TIM_OCM_PWM2);
    timer_set_oc_mode(TIM4, TIM_OC4, TIM_OCM_PWM2);

    // Habilitamos las salidas de los canales:
    timer_enable_oc_output(TIM4, TIM_OC2);
    timer_enable_oc_output(TIM4, TIM_OC3);
    timer_enable_oc_output(TIM4, TIM_OC4);

    // El timer empieza a contar:
    timer_enable_counter(TIM4);

    uint16_t pwm_val = 0;
    uint8_t state = 0;
    while (true)
    {
        switch (state)
        {
            case 0:
                timer_set_oc_value(TIM4, TIM_OC4, MAX_COUNT - pwm_val);
                timer_set_oc_value(TIM4, TIM_OC2, pwm_val++);
                break;
            case 1:
                timer_set_oc_value(TIM4, TIM_OC2, MAX_COUNT - pwm_val);
                timer_set_oc_value(TIM4, TIM_OC3, pwm_val++);
                break;
            case 2:
                timer_set_oc_value(TIM4, TIM_OC3, MAX_COUNT - pwm_val);
                timer_set_oc_value(TIM4, TIM_OC4, pwm_val++);
                break;
            default:
                state = 0;
                continue;
        }
        if (pwm_val == MAX_COUNT)
        {
            pwm_val = 0;
            state++;
        }
        delay_ms(1);
    }
}

/**
 * @brief Sub-Rutina de interrupción del SysTick (cada 1ms)
 */
void sys_tick_handler(void)
{
    millis++; // Se incrementan los millis
}