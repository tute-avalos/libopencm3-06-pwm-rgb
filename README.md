# Tutorial de programación de ARM Cortex-M con herramientas libres

Para este tutorial estoy utilizando:

* **HARDWARE:** Blue Pill (STM32F103C8) y ST-Link/V2
* **SOFTWARE:** PlatformIO en VS Code con libOpenCM3 sobre Debian GNU/Linux.

## Ejemplo 06: PWM alineado al centro

En este ejemplo se configura el TIM4 para hacer un degradé de colores con un LED RGB ánaodo común a través de las salidas OC2, OC3 y OC4 en función PWM alineado al centro.

Entrada del blog: [Programando los TIMs del STM32F1 con libOpenCM3 [Parte 2] - PWM](https://electronlinux.wordpress.com/2020/07/12/programando-los-tims-del-stm32f1-con-libopencm3-parte-2-pwm/)

Video en YouTube: 

